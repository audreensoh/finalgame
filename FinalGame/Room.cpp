#include"Room.h"
#include<iostream>
#include<string>

Room::Room()
{
	
}

Room::Room(std::string p_RoomName, std::string p_RoomDesc, int p_positionX, int p_positionY,bool p_lock)
{
	m_RoomName = p_RoomName;
	m_RoomDesc = p_RoomDesc;
	m_PositionX = p_positionX;
	m_PositionY = p_positionY;
	m_lock = p_lock;
}

Room::~Room()
{

}

void Room::setRoomName(std::string p_RoomName)
{
	m_RoomName = p_RoomName;
}

std::string Room::getRoomName()
{
	return m_RoomName;
}

void Room::setRoomDescription(std::string p_RoomDesc)
{
	m_RoomDesc = p_RoomDesc;
}

std::string Room::getRoomDescription()
{
	return m_RoomDesc;
}

void Room::addObjectToRoom(Object& p_Object)
{
	m_ObjectsInRoom.push_back(p_Object);
}

void Room::removeObjectFromRoom(std::string p_Object)
{
	bool found = false;
	for (int i = 0; i != m_ObjectsInRoom.size(); i++)
	{
		if (m_ObjectsInRoom[i].getName() == p_Object)
		{
			m_ObjectsInRoom.erase(m_ObjectsInRoom.begin() + i);
			found = true;
			break;
		}
	}
	if (found == false)
	{
		std::cout << "No item found." << std::endl;
	}

}

void Room::removeObjectFromRoom(Object& p_Object)
{
	bool found = false;
	for (int i = 0; i != m_ObjectsInRoom.size(); i++)
	{
		if (m_ObjectsInRoom[i].getName() == p_Object.getName())
		{
			m_ObjectsInRoom.erase(m_ObjectsInRoom.begin() + i);
			found = true;
		}
	}
	if (found == false)
	{
		std::cout << "No item found." << std::endl;
	}

}

std::vector<Object> Room::getAllObject()
{
	return m_ObjectsInRoom;
}

void Room::addEnemyToRoom(Enemy& p_enemy)
{
	m_EnemyInRoom.push_back(p_enemy);
}

void Room::removeEnemyFromRoom(Enemy& p_enemy)
{
	for (int i = 0; i != (m_EnemyInRoom.size()); i++)
	{
		if (m_EnemyInRoom[i].getName() == p_enemy.getName())
		{
			m_EnemyInRoom.erase(m_EnemyInRoom.begin() + i);	
			break;
		}
	}
}

//O(c)
//there is only one last enemy
void Room::removeLastEnemyInList()
{
	int num = (m_EnemyInRoom.size()) - 1;
	Enemy toRemove = m_EnemyInRoom[num];
	removeEnemyFromRoom(toRemove);
}

std::vector<Enemy> Room::getAllEnemy()
{
	return m_EnemyInRoom;
}

void Room::setPositionX(int p_positionX)
{
	m_PositionX = p_positionX;
}

int Room::getPositionX()
{
	return m_PositionX;
}

void Room::setPositionY(int p_positionY)
{
	m_PositionY = p_positionY;
}

int Room::getPositionY()
{
	return m_PositionY;
}

void Room::setLock(bool p_lock)
{
	m_lock = p_lock;
}

bool Room::getLock()
{
	return m_lock;
}

bool Room::checkObjectInRoom(std::string& p_ObjectName)
{
	bool found = false;
	for (int i = 0; i != m_ObjectsInRoom.size(); i++)
	{
		//std::cout << m_ObjectsInRoom[i].getName() << std::endl;
		if (m_ObjectsInRoom[i].getName() == p_ObjectName)
		{
			//std::cout << "Found Object" << std::endl;
			return true;
			break;
		}
	}
	if (found == false)
	{
		//std::cout << "Object cant be found" << std::endl;
		return false;
	}
	
}

Object* Room::getObject(std::string& p_ObjectName)
{
	for (int i = 0; i != m_ObjectsInRoom.size(); i++)
	{
		if (m_ObjectsInRoom[i].getName() == p_ObjectName)
		{
			return &m_ObjectsInRoom[i];
		}
	}
}

void Room::unhideObject()
{
	for (int i = 0; i != m_ObjectsInRoom.size(); i++)
	{
		m_ObjectsInRoom[i].setHidden(false);
	}
}

//to print out the current room 
void Room::printRoom()
{
	std::cout << "-------------------------------------------------------------------------------" << std::endl;
	std::cout << m_RoomName << std::endl;
	std::cout << m_RoomDesc << std::endl;
	std::cout << "x: " << m_PositionX << std::endl;
	std::cout << "y: " << m_PositionY << std::endl;
	std::cout << "-------------------------------------------------------------------------------" << std::endl;

	if (m_lock == false && m_ObjectsInRoom.size() > 0)
	{
		std::cout << "-------------------------------------------------------------------------------" << std::endl;

		for (int i = 0; i != m_ObjectsInRoom.size(); i++)
		{
			std::cout <<"Object(s):" << std::endl;
			std::cout << m_ObjectsInRoom[i].getName() << std::endl;
		}

		std::cout << "-------------------------------------------------------------------------------" << std::endl;
	}

	if (m_EnemyInRoom.size() > 0 && m_lock==false)
	{
		std::cout << "-------------------------------------------------------------------------------" << std::endl;
		
		std::cout << "Enemy(s):" << std::endl;

		for (int i = 0; i != m_EnemyInRoom.size(); i++)
		{
			std::cout << m_EnemyInRoom[i].getName() << std::endl;
		}

		std::cout << "-------------------------------------------------------------------------------" << std::endl;
	}
	
}


