#include"InputUtility.h"
#include<string>
#include<iostream>
#include<vector>
#include <locale>

using std::vector;
using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::getline;

//split the input and store each word as a string into a vector
vector<string> split(const string& s)
{
	vector<string> ret;

	typedef string::size_type string_size;
	string_size i = 0;

	// invariant: we have processed characters [original value of i, i) 
	while (i != s.size())
	{
		// ignore leading blanks
		// invariant: characters in range [original i, current i) are all spaces
		while (i != s.size() && isspace(s[i]))
			++i;

		// find end of next word
		string_size j = i;
		// invariant: none of the characters in range [original j, current j)is a space
		while (j != s.size() && !isspace(s[j]))
			j++;
		// if we found some nonwhitespace characters 
		if (i != j) {
			// copy from s starting at i and taking j - i chars
			ret.push_back(s.substr(i, j - i));
			i = j;
		}
	}

	return ret;
}

//get string input
vector<string> getInput()
{
	string line;
	cout << ">";
	getline(cin, line);
	line = stringToLower(line);
	vector<string> words = split(line);

	return words;
}

//get integer input
int getIntInput()
{
	int number;
	bool valid = false;

	while (valid == false)
	{
		cout << ">";
		cin >> number;
		if (!cin)
		{
			cin.clear();
			cout << "Only integer input allowed.Please try again." << endl;
			cin.ignore();

		}
		else
		{
			valid = true;
		}

	}
	return number;
}

//check if commands found
bool found(vector<string> i, string f)
{
	//"typedef" allows you to assign another equivalent name for a type
	//so instead of typing vector<string> i use "sentence"
	typedef vector<string> sentence;
	bool truefalse = false;
	for (sentence::iterator iter = i.begin(); iter != i.end(); ++iter)
	{
		if (*iter == f)
		{
			truefalse = true;
			return truefalse;
		}
	}
	truefalse = false;
	return truefalse;
}

//turns the whole string to lower case
std::string stringToLower(const std::string& s)
{
	std::string result;

	std::locale loc;
	for (unsigned int i = 0; i < s.length(); ++i)
	{
		result += std::tolower(s.at(i), loc);
	}

	return result;
}