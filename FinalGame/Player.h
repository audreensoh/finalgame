#ifndef PLAYER_H
#define PLAYER_H

/**
Contains Player details and attributes and functions to manage player.
**/

#include<iostream>
#include"Object.h"
#include"Inventory.h"

class Player
{
private:
	std::string m_Name;
	Inventory m_Inventory;
	Object m_Equipt;
	bool m_hasEquipt;
	int m_Xp;
	int m_level;
	int m_nextLevel;
	int m_health;
	int m_Strength;
	int m_Agility;
	int m_currentX;
	int m_CurrentY;

public:
	Player();
	Player(std::string p_Name, int p_Xp, int p_level, int p_nextLevel, bool p_hasEquipt, int p_Health, int p_Strength, int p_Agility);
	~Player();
	void setName(std::string p_Name);
	std::string getName();
	void setXp(int p_Xp);
	int getXp();
	void setLevel(int p_level);
	void addLevel();
	int getLevel();
	void setNextLevel(int p_nextLevel);
	int getNextLevel();
	void setHealth(int p_Health);
	int getHealth();
	void setStrength(int p_Strength);
	int getStrength();
	void setAgility(int p_Agility);
	int getAgility();
	void addToInventory(Object& p_Object);
	void removeFromInventory(Object& p_Object);
	void removeFromInventory(std::string p_ObjectName);
	void removeLastObjectFromInventory();
	Object getLastObjectInInventory();
	bool checkInventory(std::string& p_ObjectName);
	void setCurrentPosition(int p_x, int p_y);
	int getCurrentX();
	int getCurrentY();
	

	
	Inventory getInventory();
	bool checkObjectEquiptable(std::string& p_ObjectName);
	void printInventory();
	Object getEquipt();
	Object getObjectInInventoryByName(std::string& p_ObjectName);
	void setEquipt(std::string& p_ObjectName);
	void Player::setEquipt(Object& p_object);
	void setHasEquipt(bool p_hasEquipt);
	bool getHasEquipt();
	void removeEquipt();
	void printEquipt();

	void printPlayer();

};

#endif