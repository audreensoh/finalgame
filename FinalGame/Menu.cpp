#include"Menu.h"
#include"InputUtility.h"
#include"FightCombat.h"
#include"FileIO.h"
#include"Timer.h"
#include<vector>
#include<string>
#include<iostream>
#include <locale>
#include <ctime>

using std::cin;
using std::cout;
using std::getline;
using std::endl;
using std::vector;
using std::string;


//Main Menu to choose if player is new player or existing player
bool mainMenu(Map& p_map, Player& p_player)
{
	bool exit=false;
	while (exit == false)
	{
		enum player_choice { NEWPLAYER = 1, EXISTINGPLAYER = 2, EXIT = 3 };
		playerMenu();
		int choice = getIntInput();

		if (choice == NEWPLAYER)
		{
			return newGame(p_map, p_player);
		}
		else if (choice == EXISTINGPLAYER)
		{
			return checkPlayerExist(p_map, p_player);

		}
		else if (choice == EXIT)
		{
			return exit=true;
		}
	}
}

//to print out menu
void playerMenu()
{
	cout << "1. New Player" << endl;
	cout << "2. Existing Player" << endl;
	cout << "3. Exit" << endl;
}

//to create a new game
bool newGame(Map& p_map, Player& p_player)
{
	bool exit = false;
	system("CLS");
	p_player = newPlayer(p_player);
	loadObjects(p_map, "Objects.txt");
	loadEnemy(p_map, "Enemy.txt");
	storyLine();
	exit = startGame(p_map, p_player);
	return exit;
}

//to start the game
bool startGame(Map& p_map, Player& p_player)
{
	bool exit = false;
	do
	{
		exit = playMenu(p_map, p_player);
	} while (exit == false);
	return exit;
}

//to print current room and parsing the user input to move around or interacting with objects and enemies
bool playMenu(Map& p_map, Player& p_player)
{
	system("CLS");
	p_player.printPlayer();


	p_map.printCurrentRoom();
	checkYouthFountain(p_player);
	bool exit = parse(p_map, p_player);

	if (p_player.getXp() >= p_player.getNextLevel())
	{
		p_player.setXp(p_player.getXp() - p_player.getNextLevel());
		p_player.addLevel();
		p_player.setNextLevel(p_player.getNextLevel() + 20);
	}
	return exit;
}

//print out before game starts
void storyLine()
{
	system("CLS");
	cout << "********************************************************************************" << endl;
	cout << "*********************           Stage 1: Forest            *********************" << endl;
	cout << "*********************               Mission:               *********************" << endl;
	cout << "********************* Fight all enemies and kill the king! *********************" << endl;
	cout << "********************************************************************************" << endl;
	cout <<  endl;
	cout << "Press any key to continue..." << endl;
	cin.ignore();
}

//this creates a new player
//checks if player name exist
//player can pre-specify their own attributes
Player newPlayer(Player& p_player)
{
	bool exist = false;
	int points = 18;
		string name;
		int strength;
		int agility;
		int health;

		cout << "Enter player Name:(No Spaces allowed)" << endl;
		cout << ">";
		cin >> name;
		exist = checkIfPlayerExist(name);
		cin.clear();
	while (exist == true)
	{
		system("CLS");
		cout << "Player Name exist. Please try again." << endl;
		cout << "Enter player Name:(No Spaces allowed)" << endl;
		cout << ">";
		name = "";
		cin.clear();
		cin >> name;
		exist = checkIfPlayerExist(name);
	}

	if (exist == false)
	{
		do
		{
			points = 18;
			cout << "Please distribute 18 points between strength, Agility and health" << endl;
			cout << "Strength - 1 Point equals 1 strength (Damage)." << endl;
			cout << "Agility  - 1 Point equals 1% Agility (HitRate %)." << endl;
			cout << "Health   - 1 Point equals 10 Health." << endl;
			cout << endl;
			cout << "Strength: " << endl;
			strength = getIntInput() + 30;
			//cout << strength << endl;
			points = points - (strength - 30);
			cout << endl;
			cout << "Agility: " << "(" << points << " left)" << endl;
			agility = getIntInput() + 30;
			points = points - (agility - 30);
			cout << endl;
			cout << "Health: " << "(" << points << " left)" << endl;
			health = (getIntInput() * 10) + 100;
			points = points - ((health - 100) / 10);
			cout << endl;

			cout << "Points: " << points << endl;
			if (points < 0)
			{
				cout << "There are only 18 points to distribute, please try again." << endl;
				cin.ignore(2);
				system("CLS");
			}

		} while (points < 0);

		//cout << endl;
		cout << "Welcome Player " << name << ". This is your current stats:" << endl;
		cout << endl;
		cout << "Strength: " << strength << endl;
		cout << "Agility: " << agility << endl;
		cout << "Health: " << health << endl;

		cout << endl;
		cout << "Press any key to continue." << endl;
		cin.ignore(2);

		p_player.setName(name);
		p_player.setStrength(strength);
		p_player.setAgility(agility);
		p_player.setHealth(health);
		p_player.setXp(0);
		p_player.addLevel();
		p_player.setCurrentPosition(0, 0);
		p_player.setHasEquipt(false);
		p_player.setNextLevel(100);

		return p_player;
	}
	
	
	
}

//to parse the command inputs and functions that relates to the commands entered
bool parse(Map& p_map, Player& p_player)
{
	

	vector<string> input;

	input = getInput();
	
	if (found(input, "go"))
	{
		searchNextRoom(input, p_map, p_player);
		return false;
	}
	else if (found(input, "pick"))
	{
		pickUpObject(input, p_map, p_player);

		return false;
	}
	else if (found(input, "drop"))
	{
		dropItem(input, p_map, p_player);

		return false;
	}
	else if (found(input, "examine"))
	{
		checkToExamine(input, p_map, p_player);

		return false;
	}
	else if (found(input, "inventory"))
	{
		p_player.getInventory().printInventory();

		//cin.ignore();
		return false;

	}
	else if (found(input, "equip"))
	{
		checkToEquipt(input, p_map, p_player);
	
		return false;
	}
	else if (found(input, "equipment"))
	{
		if (p_player.getHasEquipt() == false)
		{
			cout << "You are not equipped with any equipment." << endl;
		}
		else
		{
			cout << p_player.getEquipt().getName() << endl;
		}
		

		cin.ignore();
		return false;
	}
	else if (found(input, "unlock"))
	{
		checkUnlock(p_map, p_player);
		return false;
	}
	else if (found(input, "exit"))
	{
		return checkExit(p_map, p_player);
	}
	else if (found(input, "fight"))
	{
		checkToFight(input, p_map, p_player);
		bool clear = stageClear(p_map,p_player);
		
		return clear;
	}
	else if (found(input, "rest"))
	{
		int health = p_player.getHealth();
		if (health >= 250)
		{
			cout << "Max health is 250." << endl;
		}
		else
		{
			health = p_player.getHealth() + 10;
			p_player.setHealth(health);
		}

		
		return false;
	}
	else if (found(input, "save"))
	{
		checkSave(p_map, p_player);
		return false;
	}
	else if (found(input, "help"))
	{
		printCommands();
		return false;
	}
	else
	{
		cout << "Sorry " << p_player.getName() << ", you entered an invalid comand.Please try again!" << endl;
		cin.ignore();
		return false;
	}
	

}

//To navigate and go around the map
void searchNextRoom(vector<string> p_input, Map& p_map ,Player& p_player)
{
	typedef vector<string> sentence;
	bool foundexit = false;

	for (sentence::iterator iter = p_input.begin(); iter != p_input.end(); iter++)
	{
		if ((*iter == "north") || (*iter == "n"))
		{
			p_map.moveNorth();
			p_player.setCurrentPosition(p_map.getCurrentRoom().getPositionX(), p_map.getCurrentRoom().getPositionY());
			//p_map.printCurrentRoom();
		}
		else if ((*iter == "south") || (*iter == "s"))
		{
			p_map.moveSouth();
			p_player.setCurrentPosition(p_map.getCurrentRoom().getPositionX(), p_map.getCurrentRoom().getPositionY());
			//p_map.printCurrentRoom();
		}
		else if ((*iter == "east") || (*iter == "e"))
		{
			p_map.moveEast();
			p_player.setCurrentPosition(p_map.getCurrentRoom().getPositionX(), p_map.getCurrentRoom().getPositionY());
			//p_map.printCurrentRoom();
			if (p_map.getCurrentRoom().getLock() != true)
			{
			}
		}
		else if ((*iter == "west") || (*iter == "e"))
		{
			p_map.moveWest();
			p_player.setCurrentPosition(p_map.getCurrentRoom().getPositionX(), p_map.getCurrentRoom().getPositionY());
			//p_map.printCurrentRoom();
		}
	}
	
}

//to pick up an item and put it in the inventory
void pickUpObject(std::vector<std::string> p_input, Map& p_map, Player& p_player)
{
	typedef vector<string> sentence;
	bool found=false;
	
	if (p_player.getInventory().size() >=2)
	{
		cout << "Inventory is full. (Max 2 items)" << endl;
	}
	else
	{
		for (sentence::iterator iter = p_input.begin(); iter != p_input.end(); iter++)
		{

			//cout <<p_map.getCurrentRoom().getAllObject().size()<<endl;
			//cout <<p_map.CheckIfObjectInCurrentRoom((*iter))<<endl;
			if (p_map.CheckIfObjectInCurrentRoom((*iter)) == true && p_player.getInventory().size()<2)
			{
				Object toCheck = *p_map.getObjectFromRoom(*iter);
				//cout << toCheck.getHidden() << endl;
				if (toCheck.getHidden() == false)
				{
					p_player.addToInventory(*p_map.getObjectFromRoom(*iter));
					p_map.removeObjectFromCurrentRoom(*iter);
					p_player.setXp(p_player.getXp() + 10);
					cout << toCheck.getName()<<" added to inventory." << endl;
					cin.ignore();
					found = true;
					break;
				}
				
			}


		}
	}
	
	if (p_player.getInventory().size() <2 && found == false)
	{
		cout << "Sorry. No object found with that name." << endl;
	}
	

}

//to equipt an object from inventory
void checkToEquipt(vector<string> p_input, Map& p_map, Player& p_player)
{
	typedef vector<string> sentence;

	for (sentence::iterator iter = p_input.begin(); iter != p_input.end(); iter++)
	{
		if (p_player.checkInventory(*iter)==true)
		{
			if (p_player.checkObjectEquiptable(*iter) == true)
			{
				if (p_player.getHasEquipt() == true)
				{
					Object& toReplace=p_player.getEquipt();
					p_player.setStrength(p_player.getStrength() - toReplace.getStrength());
					p_player.setAgility(p_player.getAgility() - toReplace.getAgility());
					p_player.setEquipt(*iter);
					p_player.addToInventory(toReplace);
				}
				else if (p_player.getHasEquipt() == false)
				{
					p_player.setEquipt(*iter);
				}
				cout << "You are now equipt with " << p_player.getEquipt().getName() << endl;;
				cout << "strength +" <<p_player.getEquipt().getStrength() << endl;
				cout << "Agility +" << p_player.getEquipt().getAgility() << endl;
				cin.ignore(); 
			}

		}
			
	}
}

//to fight an enemy in the room
void checkToFight(vector<string> p_input, Map& p_map, Player& p_player)
{
	typedef vector<string> sentence;

	if (p_map.CheckIfEnemyInRoom() == true)
	{
		//cout << "There is enemy in the room" << endl;
		Enemy p_Enemy = p_map.getEnemyToFight();
		//cout << p_Enemy.getName() << endl;
		if (p_Enemy.getName() == "Big King")
		{
			//make sure that players killed all enemies before they fight the king
			if (p_map.getEnemyNum() == 1)
			{
				cout << "Get ready to fight " << p_Enemy.getName() << endl;
				fight(p_map, p_Enemy, p_player);
			}
			else
			{
				cout << "Sorry.You cant fight Big King now."<<endl;
				cout <<	"You have to go to Bandit hills to kill all the bandits and return to try again." << endl;
				cin.ignore();
			}
		}
		else
		{
			cout << "Get ready to fight " << p_Enemy.getName() << endl;
			fight(p_map, p_Enemy, p_player);
		}
		
	}
	else
	{
		cout << "No enemy in this room" << endl;
	}

	if (p_player.getHealth() <= 0)
	{
		p_player.setHealth(0);
	}
}

//links to fightCombat.h to fight enemy
void fight(Map& p_map, Enemy& p_Enemy, Player& p_Player)
{
	bool exit = false;
	
	do
	{
		exit = fightOrRun(p_map, p_Enemy, p_Player);
	} while (exit == false);
		


	
		
}

//examine an object in the inventory or equipped object
void checkToExamine(vector<string> p_input, Map& p_map, Player& p_player)
{
	typedef vector<string> sentence;
	bool found=false;

	for (sentence::iterator iter = p_input.begin(); iter != p_input.end(); iter++)
	{
		if (p_player.checkInventory(*iter) == true )
		{
			Object toExamine=p_player.getObjectInInventoryByName(*iter);
			cout << "Item: " << toExamine.getName() << endl;
			cout << "Strength: " << toExamine.getStrength() << endl;
			cout << "Agility: " << toExamine.getAgility() << endl;
			found = true;
		}
		else if (p_player.getEquipt().getName() == (*iter))
		{
			Object toExamine = p_player.getEquipt();
			cout << "Item: " << toExamine.getName() << endl;
			cout << "Strength: " << toExamine.getStrength() << endl;
			cout << "Agility: " << toExamine.getAgility() << endl;
			found = true;
		}

	}

	cin.ignore();
	if (found == false)
		{
			cout << "No item found in inventory or equiptment by that name." << endl;
		}
}

//drop an item from inventory or an equipped item back to the room
void dropItem(vector<string> p_input, Map& p_map, Player& p_player)
{
	typedef vector<string> sentence;

	for (sentence::iterator iter = p_input.begin(); iter != p_input.end(); iter++)
	{
		if (p_player.getInventory().size() > 0 && p_player.checkInventory(*iter) == true)
		{
			Object toDrop = p_player.getObjectInInventoryByName(*iter);
			//cout << toDrop.getXPosition() << "  " << toDrop.getYPosition() << endl;
			p_map.putBackObject(toDrop);
			p_player.removeFromInventory(toDrop);
			cout << "Item removed from inventory" << endl;
		}
		else if (p_player.getEquipt().getName()==(*iter))
		{
			Object toDrop = p_player.getEquipt();
			p_map.putBackObject(toDrop);
			p_player.removeEquipt();
			cout << "Equipment removed" << endl;
		}
		
	}cin.ignore();
}

//check if player name exists
bool checkIfPlayerExist(string name)
{
	vector<string> allNames = getExistingPlayer();
	for (int i = 0; i != allNames.size(); i++)
	{
		if (name == allNames[i])
		{
			return true;
			break;
		}
	}
	return false;

}

//saves player to its own particular player, object and enemy folder
void checkSave(Map& p_map, Player& p_player)
{
	Timer myTimer;
	myTimer.Reset();
	string fileName = p_player.getName() + ".txt";
	string objectFile = p_player.getName() + "Object.txt";
	string EnemyFile = p_player.getName() + "Enemy.txt";
	savePlayerToFile(p_map, p_player, fileName);
	saveObjectsStillInMap(p_map, objectFile);
	saveEnemyStillInMap(p_map, EnemyFile);
	cout << "Player Saved." << endl;
	cout << "time taken(Ms): " << myTimer.GetMS() << endl;
	cin.ignore();
}

//load existing players from their respective name, object and enemy text file
bool checkPlayerExist(Map& p_map, Player& p_player)
{
	bool exit = false;
	std::string name;
	cout << "Enter player Name:(No Spaces allowed)" << endl;
	cout << ">";
	cin >> name;

	bool exist = checkIfPlayerExist(name);
	if (exist == true)
	{
		Timer myTimer;
		myTimer.Reset();
		string fileName = name + ".txt";
		string objectfile = name + "Object.txt";
		string enemyfile = name + "Enemy.txt";
		loadPlayerFromFile(p_map, p_player, fileName);
		loadObjects(p_map, objectfile);
		loadEnemy(p_map, enemyfile);
		cout << "time taken(Ms): " << myTimer.GetMS() << endl;
		cin.ignore();
		exit = startGame(p_map, p_player);
	}
	else
	{
		cout << "Sorry. No existing player by the name " << name << endl;
		cin.ignore(2);
		exit = false;
	}
	return exit;
}

//check if there is a key in the inventory to unlock the castle
void checkUnlock(Map& p_map, Player& p_player)
{
	string keyName = "old_castle_key";
	if (p_map.getCurrentRoom().getLock() == true && p_player.checkInventory(keyName) == true)
	{
		p_map.unlockCurrentRoom();
		string desc = "Castle is unlocked. Welcome to the castle, There is something big waiting for you inside";
		p_map.setCurrentRoomDesc(desc);
		p_player.removeFromInventory(keyName);

		p_map.unhiddenObject();

		cout << p_map.getCurrentRoom().getRoomName() << "is unlocked!" << endl;
		cin.ignore();
	}
	else if (p_player.checkInventory(keyName) == false)
	{
		cout << "You dont have key in your inventory" << endl;
		cin.ignore();
	}
	else if (p_map.getCurrentRoom().getLock() == false)
	{
		cout << "This room is already unlocked." << endl;
		cin.ignore();
	}
}

//function when exit command is entered
bool checkExit(Map& p_map, Player& p_player)
{
	cout << "Are you sure you want to quit?(y/n)" << endl;
	string quit;
	cin >> quit;
	quit=stringToLower(quit);
	if (quit == "y" || quit == "yes")
	{
		cout << "Do you want to save game?(y/n)" << endl;
		string save;
		cin >> save;
		save = stringToLower(save);
		if (save == "y" || save == "yes")
		{
			checkSave(p_map, p_player);
		}
		return true;
	}
	else if (quit == "n" || quit == "no")
	{
		return false;
	}
}

//if current room is the youth fountain the player will have a +50 health
void checkYouthFountain(Player& p_player)
{
	if (p_player.getCurrentX() == 3 && p_player.getCurrentY() == 1)
	{
		cout << "Player health +50" << endl;
		cin.ignore();
		p_player.setHealth(p_player.getHealth() + 50);

	}
}

//commands to help player through the game
void printCommands()
{
	cout << "go (Direction)   -To move around along the map. ie. go north. " << endl;
	cout << "pick (Object)    -To pick up an object. ie. pick up villager_axe" << endl;
	cout << "drop (Object)    -To drop an object in inventory or object which is equipped. " << endl;
	cout << "                  ie. drop villager_axe" << endl;
	cout << "examine (Object) -To examine an object in inventory or object which is " << endl;
	cout << "                  equipped.ie. examine villager_axe" << endl;
	cout << "equip (Object)   -To equip an object which is in the inventory, to improve" << endl;
	cout << "                  strength and agility. ie. equip villager_axe" << endl;
	cout << "inventory        -To check the items in your inventory." << endl;
	cout << "equipment        -To check object which is currently equipped." << endl;
	cout << "rest             -To chill and Player's health +10" << endl;
	cout << "unlock           -To unlock a particular room which is locked. Make sure you " << endl;
	cout << "                  have a key in your inventory." << endl;
	cout << "fight            -To fight with enemy." << endl;
	cout << "save             -To save game at current state." << endl;
	cout << "exit             -To exit game." << endl;
	cout << "run              -To leave fight in the middle of a fight.(Can only be used" << endl;
	cout << "                 in fight situation)" << endl;
	cin.ignore();
}

//when the game comes to an end
bool stageClear(Map& p_map, Player& p_player)
{
	if (p_map.getEnemyNum() == 0)
	{
		system("CLS");
		cout << "***********************************************************************" << endl;
		cout << "*************************  Congratulations!!  *************************" << endl;
		cout << "*************************    Stage 1 clear    *************************" << endl;
		cout << "***********************************************************************" << endl;
		cout << endl;
		cout << "Do you want to save game?(y/n)" << endl;
		string save;
		cin >> save;
		save = stringToLower(save);
		if (save == "y" || save == "yes")
		{
			checkSave(p_map, p_player);
		}
		cout << "Goodbye..." << endl;
		cin.ignore();
		return true;
	}
	return false;


}