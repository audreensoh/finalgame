#ifndef MAP_H
#define MAP_H

/**
Contains a map which includes functions for managing the rooms, objects, and enemy
**/

#include"Room.h"
#include"Object.h"
#include<fstream>

class Map
{
private:
	int m_lengthX;
	int m_heightY;
	int m_currentX;
	int m_CurrentY;
	int m_count;
	int m_EnemyNum;
	int m_ObjectNum;
	Room m_room[16];
	Room* m_currentRoom;
public:
	Map();
	Map(Room room[16]);
	~Map();

	void addRoom(Room p_room);
	bool isWall(int p_X, int p_Y);
	void putObjectInRoom(int p_x,int p_y, std::string p_roomName, Object p_object);
	void putEnemyToRoom(int p_x, int p_y, std::string p_roomName, Enemy p_enemy);
	void setCurrentPosition(int p_x, int p_y);
	void setLength(int p_length);
	void setHeight(int p_height);
	Room getCurrentRoom();
	void printCurrentRoom();

	void loadRooms(std::ifstream& inFile);
	void loadObjects(std::ifstream& inFile);
	void loadEnemy(std::ifstream& inFile);

	bool CheckIfObjectInCurrentRoom(std::string& p_ObjectName);
	Object* getObjectFromRoom(std::string& p_ObjectName);
	void removeObjectFromCurrentRoom(std::string& p_ObjectName);
	void unlockCurrentRoom();
	void unhiddenObject();
	void putBackObject(Object p_Object);
	void putBackEnemy(Enemy p_enemy);
	void setCurrentRoomDesc(std::string& p_roomDesc);
	void setCastleLockDesc(bool lock, std::string p_Desc);
	
	bool CheckIfEnemyInRoom();
	void removeLastEnemyfromRoom();
	Enemy getEnemyToFight();

	Room* getAllRooms();
	int getEnemyNum();
	int getObjectNum();
	

	void moveNorth();
	void moveSouth();
	void moveWest();
	void moveEast();
};


#endif