#ifndef OBJECT_H
#define OBJECT_H

/**
Contains Object details and attributes.
**/

#include<iostream>

class Object
{
private:
	std::string m_Name;
	std::string m_Description;
	std::string m_room;
	int m_xPosition;
	int m_yPosition;
	bool m_Equiptable;
	int m_Strength;
	int m_Agility;
	bool m_hidden;
public:
	Object();
	Object(std::string p_Name, std::string p_Description, std::string p_Room, int p_XPosition, int p_YPosition, bool p_Equiptable, int p_Strength, int p_Agility,bool hidden);
	~Object();
	void setName(std::string p_Name);
	std::string getName();
	void setDescription(std::string p_Description);
	std::string getDescription();
	void setRoom(std::string p_Room);
	std::string getRoom();
	void setEquiptable(bool p_Equiptable);
	bool getEquiptable();
	void setStrength(int p_Strength);
	int getStrength();
	void setAgility(int p_Agility);
	int getAgility();
	void setXPosition(int p_XPosition);
	int getXPosition();
	void setYPosition(int p_YPosition);
	int getYPosition();
	void setHidden(bool p_hidden);
	bool getHidden();
};

#endif