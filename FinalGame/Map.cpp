#include"Map.h"
#include<fstream>
#include<iostream>
#include<string>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::getline;
using std::ifstream;
using std::ofstream;

//default constructor
Map::Map()
{

}

//constructor
Map::Map(Room room[16])
{
	for (int i = 0; i != 16; i++)
	{
		m_room[i] = room[i];
		m_count++;
	}
	
}

//destructor
Map::~Map()
{

}

//add room to map
void Map::addRoom(Room p_room)
{
	m_room[m_count] = p_room;
	m_count++;
	//cout << "Room Added: " << p_room.getRoomName() << endl;
	
}

//to check if its the wall/end of map
bool Map::isWall(int p_X, int p_Y)
{
	if ((p_X >= m_lengthX) && (p_Y >= m_heightY) && (p_X <= 0) && (p_Y <= 0))
	{
		return true;
	}
	else
	{
		return false;
	}
}

//put object in room
//O(n)
//it needs to go through all the rooms
void Map::putObjectInRoom(int p_x, int p_y, std::string p_roomName, Object p_object)
{
	for (int i = 0; i != 16; i++)
	{
		if ((m_room[i].getPositionX() == p_x) && (m_room[i].getPositionY() == p_y) && (m_room[i].getRoomName() == p_roomName))
		{
			m_room[i].addObjectToRoom(p_object);
			//cout << p_object.getName() << " Added to " << p_roomName << endl;
			m_ObjectNum++;
		}
	}
}

//put enemy in room
//O(n)
//it needs to go through all the rooms
void Map::putEnemyToRoom(int p_x, int p_y, std::string p_roomName, Enemy p_enemy)
{
	for (int i = 0; i != 16; i++)
	{

		if ((m_room[i].getPositionX() == p_x) && (m_room[i].getPositionY() == p_y) && (m_room[i].getRoomName() == p_roomName))
		{
			m_room[i].addEnemyToRoom(p_enemy);
			m_EnemyNum++;
		}
	}
}

//get current room
Room Map::getCurrentRoom()
{
	return *m_currentRoom;
}

//print current room
void Map::printCurrentRoom()
{
	m_currentRoom->printRoom();
}

//load room from text file
//not used as there is fileIO.h
void Map::loadRooms(ifstream& inFile)
{
	string roomName = "";
	string roomDesc = "";
	int x;
	int y;
	bool lock = 0;
	string garbage = "";

	for (int i = 0; i != 16; i++)
	{

		Room roomToLoad;

		inFile >> garbage;
		getline(inFile, roomName);
		inFile >> garbage;
		getline(inFile, roomDesc);
		inFile >> garbage >> x;
		inFile >> garbage >> y;
		inFile >> garbage >> lock;

		roomToLoad.setRoomName(roomName);
		roomToLoad.setRoomDescription(roomDesc);
		roomToLoad.setPositionX(x);
		roomToLoad.setPositionY(y);
		roomToLoad.setLock(lock);

		addRoom(roomToLoad);

	}
}

//not used as there is fileIO.h
void Map::loadObjects(std::ifstream& inFile)
{
	//cout << "Loading objects load" << endl;
	std::string name;
	std::string desc;
	std::string room;
	int positionX;
	int PositionY;
	bool equiptable;
	int strength;
	int agility;
	bool hidden;
	std::string garbage;
	int totalNumOfObject;
	
	inFile >> garbage >> totalNumOfObject;
	for (int i = 0; i != totalNumOfObject; i++)
	{
		inFile >> garbage >> name;
		inFile >> garbage;
		getline(inFile, desc);
		inFile >> garbage;
		getline(inFile, room);
		inFile >> garbage >> positionX;
		inFile >> garbage >> PositionY;
		inFile >> garbage >> equiptable;
		inFile >> garbage >> strength;
		inFile >> garbage >> agility;
		inFile >> garbage >> hidden;

		//cout << hidden << endl;
		Object loadObject(name, desc, room, positionX, PositionY, equiptable, strength, agility,hidden);
		//cout << name << " and " << room << endl;
		putObjectInRoom(positionX, PositionY, room, loadObject);
		
	}

	setCurrentPosition(0, 0);
	m_lengthX = 3;
	m_heightY = 3;
}

//not used as there is fileIO.h
void Map::loadEnemy(std::ifstream& inFile)
{

	//cout << "Loading objects load" << endl;
	std::string name;
	std::string desc;
	std::string room;
	int positionX;
	int PositionY;
	int strength;
	int agility;
	int health;
	std::string garbage;

	//cout << totalNumOfObject << endl;
	for (int i = 0; i != 3; i++)
	{

		inFile >> garbage;
		getline(inFile, name);
		inFile >> garbage;
		getline(inFile, room);
		inFile >> garbage >> positionX;
		inFile >> garbage >> PositionY;
		inFile >> garbage >> strength;
		inFile >> garbage >> agility;
		inFile >> garbage >> health;

		//cout << hidden << endl;
		Enemy enemy(name, health, strength, agility, positionX, PositionY,room);
		//cout << name << " and " << room << endl;
		putEnemyToRoom(positionX, PositionY, room, enemy);

	}
}

//using name of object to check if object is in current room
//O(n)
//it needs to go through all the objects
bool Map::CheckIfObjectInCurrentRoom(string& p_ObjectName)
{
	return (*m_currentRoom).checkObjectInRoom(p_ObjectName);
}

//to get an object in current room
//O(n)
//it needs to go through all the objects in the room
Object* Map::getObjectFromRoom(std::string& p_ObjectName)
{
	return (*m_currentRoom).getObject(p_ObjectName);	
}

//set current player in room position
//O(n)
//it needs to go through all the rooms
void Map::setCurrentPosition(int p_x, int p_y)
{
	
	for (int i = 0; i != 16; i++)
	{
		if ((m_room[i].getPositionX() == p_x) && (m_room[i].getPositionY() == p_y))
		{
			//cout << "Current room" << m_room[i].getRoomName();
			m_currentRoom = &m_room[i];
			m_currentX = p_x;
			m_CurrentY = p_y;
		}
	}
}

//set the length of map
void Map::setLength(int p_length)
{
	m_lengthX = p_length;
}

//set the height of map
void Map::setHeight(int p_height)
{
	m_heightY = p_height;
}

//remove an object from room by using the object name
void Map::removeObjectFromCurrentRoom(std::string& p_ObjectName)
{
	bool found=(*m_currentRoom).checkObjectInRoom(p_ObjectName);
	if (found == true)
	{
		(*m_currentRoom).removeObjectFromRoom(p_ObjectName);
		m_ObjectNum--;
	}
	
}

//unlock current room
void Map::unlockCurrentRoom()
{
	(*m_currentRoom).setLock(false);
}

//to unhide the objects in the room if room is unlocked
void Map::unhiddenObject()
{
	(*m_currentRoom).unhideObject();
}

//to put object back to room
//advanced function for putObjectInRoom()
void Map::putBackObject(Object p_Object)
{
	//cout << p_Object.getName() << endl;
	//cout << "Position x: " << p_Object.getXPosition() << endl;
	//cout << "Position y: " << p_Object.getYPosition() << endl;
	putObjectInRoom((p_Object.getXPosition()), p_Object.getYPosition(), p_Object.getRoom(), p_Object);
}

//to put enemy back to room
//advanced function for putEnemyToRoom()
void Map::putBackEnemy(Enemy p_enemy)
{
	putEnemyToRoom(p_enemy.getX(), p_enemy.getY(), p_enemy.getRoomName(), p_enemy);
}

//to change current room description
void Map::setCurrentRoomDesc(std::string& p_roomDesc)
{
	m_currentRoom->setRoomDescription(p_roomDesc);
}

//O(n)
//it needs to go through all the rooms
void Map::setCastleLockDesc(bool lock, std::string p_Desc)
{
	for (int i = 0; i != 16; i++)
	{
		if (m_room[i].getPositionX()==3 && m_room[i].getPositionY()==0)
		{
			//cout << "setting castle lock" << lock << p_Desc << endl;
			m_room[i].setLock(lock);
			m_room[i].setRoomDescription(p_Desc);
			break;
		}
	}
}

//to check if there are any enemy(ies) in the room
bool Map::CheckIfEnemyInRoom()
{
	if (m_currentRoom->getAllEnemy().size() >= 1)
	{
		return true;
	}
	return false;
}	

//to remove the last enemy in the room
//O(c)
//shown in room.cpp
void Map::removeLastEnemyfromRoom()
{
	m_currentRoom->removeLastEnemyInList();
	m_EnemyNum--;
}

//get the last enemy in the room to fight with player
//O(c)
//using the size of enemy vectore to get the last enemy in the list
Enemy Map::getEnemyToFight()
{
	int size = m_currentRoom->getAllEnemy().size()-1;
	//cout << "Total enemy in map: " << m_EnemyNum << endl;;
	//cout << "Current Enemy Size: " << m_currentRoom->getAllEnemy().size() << endl;
	std::vector<Enemy> toReturn = m_currentRoom->getAllEnemy();
	return toReturn[size];
}

//To get all rooms in the map
Room* Map::getAllRooms()
{
	return m_room;
}

//get the number of enemies in the map
int Map::getEnemyNum()
{
	return m_EnemyNum;
}

//get the number of objects in the map
int Map::getObjectNum()
{
	return m_ObjectNum;
}

//navigation functions
void Map::moveNorth()
{
	int y = m_CurrentY + 1;
	//cout << "move to x: " << m_currentX << endl;
	//cout << "        y: " << y << endl;
	if (y > m_heightY)
	{
		cout << "You on the very north already." << endl;
		cin.ignore();
	}
	else
	{
		setCurrentPosition(m_currentX, y);
	}
	
}
void Map::moveSouth()
{
	int y = m_CurrentY - 1;

	if (y < 0)
	{
		cout << "You are on the very south already." << endl;
		cin.ignore();
	}
	else
	{
		setCurrentPosition(m_currentX, y);
	}
}

void Map::moveWest()
{
	int x = m_currentX - 1;
	if (x < 0)
	{
		cout << "You are on the very west already." << endl;
		cin.ignore();
	}
	else
	{
		setCurrentPosition(x, m_CurrentY);
	}
	
}

void Map::moveEast()
{
	int x = m_currentX + 1;
	if (x > m_lengthX)
	{
		cout << "You are on the very east already." << endl;
		cin.ignore();
	}
	else
	{
		setCurrentPosition(x, m_CurrentY);
	}
	
}