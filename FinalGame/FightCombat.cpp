#include"FIghtCombat.h"
#include"InputUtility.h"
#include<string>
#include<iostream>
#include<vector>

using std::cin;
using std::vector;
using std::cout;
using std::endl;
using std::string;

//To get random percentage if player or enemy hits the opposition double
//depends on the agility of player
//enemy agility is fixed at 20 and king at 50
bool getRandomPercentage(int p_Agility)
{
	int randomNumber = rand() % 100;

	if (randomNumber <= p_Agility && randomNumber > 0)
	{
		//cout << "hit double" << randomNumber << endl;
		return true;
	}
	else
	{
		//cout << "hit single" << randomNumber << endl;
		return false;
	}
	
}

//50% rate to hit or miss
bool hitOrMiss()
{
	int randomNumber = rand() % 101;
	
	if (randomNumber <=50)
	{
		//cout << "hit" <<randomNumber <<endl;
		return true;
	}
	else if (randomNumber > 50)
	{
		//cout << "missed" << randomNumber << endl;
		return false;
	}
}

//this function is where all the fight happens
bool combat(Enemy& p_enemy, Player& p_player)
{


	bool playerDoublehit = getRandomPercentage(p_player.getAgility());
	bool EnemyDoubleHit = getRandomPercentage(p_enemy.getAgility());
	bool playerHit = hitOrMiss();
	bool EnemyHit = hitOrMiss();
	bool enemyDead = false;

	if (playerHit == true)
	{
		
		if (playerDoublehit == true)
		{
			p_enemy.setHealth(p_enemy.getHealth() - (p_player.getStrength()*2));
			cout << "You hit the enemy by double." << endl;
			if (p_enemy.getHealth() <= 0)
			{
				//check to see if enemy is dead, if its not, then proceed to the function
				//where the enemy gets to hit the player
				enemyDead= true;
			}
			return false;
		}
		else if (playerDoublehit == false)
		{
			p_enemy.setHealth(p_enemy.getHealth() - p_player.getStrength());
			cout << "You hit the enemy." << endl;
			if (p_enemy.getHealth() <= 0)
			{
				enemyDead= true;
			}
			enemyDead= false;
		}
	}
	else
	{
		enemyDead= false;
	}

	if (enemyDead == false)
	{
		if (EnemyHit == true)
		{

			if (EnemyDoubleHit == true)
			{
				p_player.setHealth(p_player.getHealth() - (p_enemy.getStrength() * 2));
				cout << "The enemy hit you by double." << endl;
			}
			else if (EnemyDoubleHit == false)
			{
				p_player.setHealth(p_player.getHealth() - p_enemy.getStrength());
				cout << "The enemy hit you." << endl;
			}
		}
		else
		{
		}
	}


}

//this function is to parse the input from player if they want to fight
//or run away in the middle of a fight
//if they run the enemy is put back to the map with the current life(so it wont be always 100 when the player comes back to fight again)
bool fightOrRun(Map& p_map,Enemy& p_enemy, Player& p_player)
{
	std::vector<string> input;
	bool exit = false;

	cout << p_player.getName() << ": " << p_player.getHealth() << endl;
	cout << p_enemy.getName() << ": " << p_enemy.getHealth() << endl;

	cout << "Choose to fight or run" << endl;
	input = getInput();

	if (found(input, "fight"))
	{
		combat(p_enemy, p_player);
		
		if (p_enemy.getHealth() <= 0)
		{
			p_map.removeLastEnemyfromRoom();
			p_player.setXp(p_player.getXp() + 60);
			cout << "You won!!" << endl;
			cout << "Xp +60" << endl;
			cin.ignore();
			exit = true;
		}
		else if (p_player.getHealth() <= 0)
		{
			//player dies
			p_player.setHealth(0);
			if (p_player.getHasEquipt() == true)
			{
				//if the player is equipped then it will drop the equipment back to room
				Object toDrop = p_player.getEquipt();
				p_map.putBackObject(toDrop);
				p_player.removeEquipt();
				p_player.setHealth(0);
				cout << p_enemy.getName() << " won!!" << endl;
				cout << "Item dropped can be picked up again at the room it used to be in." << endl;
				cin.ignore();
			}
			else if (p_player.getInventory().size() != 0)
			{
				//if there is no equipment then it will drop the last thing in the inventory back to room
				Object toPutBack=p_player.getLastObjectInInventory();

				p_player.removeLastObjectFromInventory();
				p_map.putBackObject(toPutBack);
			}

			//here it removes the enemy and put back the current enemy which maybe 
			//have a life not at 100
			Enemy toPutBack = p_enemy;
			p_map.removeLastEnemyfromRoom();
			p_map.putBackEnemy(toPutBack);
			exit = true;
		}
	}
	else if (found(input, "run"))
	{
		//here it removes the enemy and put back the current enemy which maybe 
		//have a life not at 100
		Enemy toPutBack = p_enemy;
		p_map.removeLastEnemyfromRoom();
		p_map.putBackEnemy(toPutBack);
		exit = true;
	}
	return exit;
}



