#include"Object.h"
#include<string>

//default constructor
Object::Object()
{
}

//constructor
Object::Object(std::string p_Name, std::string p_Description, std::string p_Room, int p_XPosition, int p_YPosition, bool p_Equiptable, int p_Strength, int p_Agility,bool p_hidden)
{
	m_Name = p_Name;
	m_Description = p_Description;
	m_room = p_Room;
	m_Equiptable = p_Equiptable;
	m_Strength = p_Strength;
	m_Agility = p_Agility;
	m_hidden = p_hidden;
	m_xPosition = p_XPosition;
	m_yPosition = p_YPosition;
}

//destructor
Object::~Object()
{}

//getters and setters
void Object::setName(std::string p_Name)
{
	m_Name = p_Name;
}

std::string Object::getName()
{
	return m_Name;
}

void Object::setDescription(std::string p_Description)
{
	m_Description = p_Description;
}

std::string Object::getDescription()
{
	return m_Description;
}

void Object::setRoom(std::string p_Room)
{
	m_room = p_Room;
}

std::string Object::getRoom()
{
	return m_room;
}

void Object::setEquiptable(bool p_Equiptable)
{
	m_Equiptable = p_Equiptable;
}

bool Object::getEquiptable()
{
	return m_Equiptable;
}

void Object::setStrength(int p_Strength)
{
	m_Strength = p_Strength;
}

int Object::getStrength()
{
	return m_Strength;
}

void Object::setAgility(int p_Agility)
{
	m_Agility = p_Agility;
}

int Object::getAgility()
{
	return m_Agility;
}

void Object::setXPosition(int p_XPosition)
{
	m_xPosition = p_XPosition;
}

int Object::getXPosition()
{
	return m_xPosition;
}

void Object::setYPosition(int p_YPosition)
{
	m_yPosition = p_YPosition;
}

int Object::getYPosition()
{
	return m_yPosition;
}

void Object::setHidden(bool p_hidden)
{
	m_hidden = p_hidden;
	std::cout << m_Name << ": " << m_hidden << std::endl;
}

bool Object::getHidden()
{
	return m_hidden;
}