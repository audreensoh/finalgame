#ifndef FIGHTCOMBAT_H
#define FIGHTCOMBAT_H

/**
Contains functions for Player fighting enemy
**/

#include"Enemy.h"
#include"Player.h"
#include"Map.h"

bool getRandomPercentage(int p_Agility);
bool hitOrMiss();
bool combat(Enemy& p_enemy, Player& p_player);
bool fightOrRun(Map& p_map, Enemy& p_enemy, Player& currentPlayer);
std::vector<std::string> getInput();
std::string stringToLower(const std::string& s);
#endif