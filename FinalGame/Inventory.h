#ifndef INVENTORY_H
#define INVENTORY_H

/**
Inventory which contains object belongs to player.
**/

#include<string>
#include<vector>
#include "Object.h"

class Inventory
{
private:
	std::vector<Object> m_object;

public:
	Inventory();
	Inventory(std::vector<Object>& objects);
	~Inventory();

	void addToInventory(Object& p_Object);
	void removeFromInventory(Object& p_Object);
	bool removeFromInventory(std::string p_ObjectName);
	void removeLast();
	Object getLast();
	bool checkInInventory(std::string p_ObjectName);

	Object getObjectByName(std::string p_ObjectName);

	int size();
	std::vector<Object> getObjects();
	void printInventory();

};

#endif