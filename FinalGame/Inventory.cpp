#include"Inventory.h"
#include<iostream>
#include<fstream>

using std::vector;
using std::cin;
#include"Inventory.h"
#include<iostream>
#include<fstream>

using std::vector;
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::ofstream;
//using namespace std;

//default constructor
Inventory::Inventory()
{

}

//constructor
Inventory::Inventory(vector<Object>& p_Object)
{
	m_object = p_Object;
}

//destructor
Inventory::~Inventory()
{

}

//add an object to inventory
void Inventory::addToInventory(Object& p_Object)
{
	if (m_object.size() >= 2)
	{
		cout << "You can only have maximum two items in your inventory" << endl;
	}
	else
	{
		m_object.push_back(p_Object);
	}
	
}

//remove an object from inventory
void Inventory::removeFromInventory(Object& p_Object)
{
	bool foundItem = false;
	for (vector<Object>::size_type i = 0; i != m_object.size(); i++)
	{
		if (m_object[i].getName() == p_Object.getName())
		{
			m_object.erase(m_object.begin() + i);
			foundItem = true;
			break;
		}
	}
	if (foundItem == false)
	{
		cout << "Object not found in inventory." << endl;
	}
}

//remove an object from inventory using object name and 
//loop through inventory to get if an object name equals to the string searched.
bool Inventory::removeFromInventory(string p_ObjectName)
{
	bool foundItem = false;
	for (vector<Object>::size_type i = 0; i != m_object.size(); i++)
	{
		if (m_object[i].getName() == p_ObjectName)
		{
			m_object.erase(m_object.begin() + i);
			foundItem = true;
			break;
		}
	}
	if (foundItem == false)
	{
		cout << "Object not found in inventory." << endl;
		
	}
	return foundItem;
}

//remove the last object in the inventory
void Inventory::removeLast()
{
	int size = m_object.size() - 1;
	m_object.erase(m_object.begin() + size);

}

//get last object from inventory
Object Inventory::getLast()
{
	int size = m_object.size() - 1;
	return m_object[size];
}

//check if an item is in inventory 
bool Inventory::checkInInventory(string p_ObjectName)
{
	bool foundItem = false;
	for (vector<Object>::size_type i = 0; i != m_object.size(); i++)
	{
		if (m_object[i].getName() == p_ObjectName)
		{
			//cout << "Found item" << endl;
			foundItem = true;
			break;
		}
	}
	if (foundItem == false)
	{
		//cout << "Checked but no object found" << endl;
	}
	return foundItem;
}

//search and get an object in the inventory by name
Object Inventory::getObjectByName(string p_ObjectName)
{
	bool foundItem = false;
	for (vector<Object>::size_type i = 0; i != m_object.size(); i++)
	{
		if (m_object[i].getName() == p_ObjectName)
		{
			foundItem = true;
			
			return m_object[i];
			break;
		}
	}
	if (foundItem == false)
	{
		//cout << "no object found" << endl;	
	}
}

//inventory size
int Inventory::size()
{
	return m_object.size();
}

//get all objects in inventory
vector<Object> Inventory::getObjects()
{
	return m_object;
}

//print inventory
void Inventory::printInventory()
{
	cout << "Inventory:" << endl;
	for (vector<Object>::size_type i = 0; i != m_object.size(); i++)
	{
		cout << "Item " << i + 1 << " : " << m_object[i].getName() << endl;
	}
}
