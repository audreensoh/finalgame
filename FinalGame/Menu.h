#ifndef MENU_H
#define MENU_H

/**
Contains functions for user interface including main menu, parsing input
Most of user interface functions are in here.
**/

#include"Enemy.h"
#include"Inventory.h"
#include"Map.h"
#include"Object.h";
#include"Player.h"
#include"Room.h"
#include"FileIO.h"

bool mainMenu(Map& p_map, Player& p_player);
Player newPlayer(Player& p_player);
bool playMenu(Map& p_map, Player& p_player);
bool parse(Map& currentMap, Player& currentPlayer);
void searchNextRoom(std::vector<std::string> p_input, Map& p_map, Player& p_player);
void pickUpObject(std::vector<std::string> p_input, Map& p_map, Player& p_player);
void checkToEquipt(std::vector<std::string> p_input, Map& p_map, Player& p_player);
void checkToExamine(std::vector<std::string> p_input, Map& p_map, Player& p_player);
void checkToFight(std::vector<std::string> p_input, Map& p_map, Player& p_player);
void fight(Map& p_map,Enemy& p_Enemy, Player& p_player);
void dropItem(std::vector<std::string> p_input, Map& p_map, Player& p_player);
bool checkIfPlayerExist(std::string name);
void checkSave(Map& p_map, Player& p_player);
bool checkPlayerExist(Map& p_map, Player& p_player);
void checkUnlock(Map& p_map, Player& p_player);
bool checkExit(Map& p_map, Player& p_player);
void checkYouthFountain(Player& p_player);
void printCommands();

void playerMenu();
bool newGame(Map& p_map, Player& p_player);
bool startGame(Map& p_map, Player& p_player);
void storyLine();
bool stageClear(Map& p_map, Player& p_player);
#endif