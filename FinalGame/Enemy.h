#ifndef ENEMY_H
#define ENEMY_H

/**
contains all enemy details and attributes.
**/

#include<iostream>

class Enemy
{
private:
	std::string m_Name;
	int m_health;
	int m_Strength;
	int m_Agility;
	int m_x;
	int m_y;
	std::string m_RoomName;
public:
	Enemy();
	Enemy(std::string p_Name, int p_health, int p_Strength, int p_Agility, int p_x, int p_y, std::string room);
	~Enemy();
	void setName(std::string p_Name);
	std::string getName();
	void setHealth(int p_Health);
	int getHealth();
	void setStrength(int p_Strength);
	int getStrength();
	void setAgility(int p_Agility);
	int getAgility();
	int getX();
	int getY();
	std::string getRoomName();
};

#endif