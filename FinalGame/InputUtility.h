#ifndef INPUTUTILITY_H
#define INPUTUTILITY_H

/**
Contains functions for getting input and parsing input.
**/

#include<vector>

std::vector<std::string> getInput();
int getIntInput();
std::vector<std::string> split(const std::string& s);
bool found(std::vector<std::string> i, std::string f);
std::string stringToLower(const std::string& s);

#endif