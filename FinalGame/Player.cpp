#include"Player.h"
#include<iostream>
#include<string>

Player::Player()
{

}

Player::Player(std::string p_Name, int p_Xp, int p_level, int p_nextLevel, bool p_hasEquipt, int p_Health, int p_Strength, int p_Agility)
{
	m_Name = p_Name;
	m_Xp = p_Xp;
	m_level = p_level;
	m_nextLevel = p_nextLevel;
	m_health = p_Health;
	m_Strength = p_Strength;
	m_Agility = p_Agility;
	m_hasEquipt = p_hasEquipt;

}

Player::~Player()
{

}

void Player::setName(std::string p_Name)
{
	m_Name = p_Name;
}

std::string Player::getName()
{
	return m_Name;
}

void Player::setXp(int p_Xp)
{
	m_Xp = p_Xp;
}

int Player::getXp()
{
	return m_Xp;
}

void Player::setLevel(int p_level)
{
	m_level = p_level;
}

void Player::addLevel()
{
	m_level++;
}

int Player::getLevel()
{
	return m_level;
}

void Player::setNextLevel(int p_nextLevel)
{
	m_nextLevel=p_nextLevel;
}

int Player::getNextLevel()
{
	return m_nextLevel;
}

void Player::setHealth(int p_Health)
{
	m_health = p_Health;
}

int Player::getHealth()
{
	return m_health;
}

void Player::setStrength(int p_Strength)
{
	m_Strength = p_Strength;
}

int Player::getStrength()
{
	return m_Strength;
}

void Player::setAgility(int p_Agility)
{
	m_Agility = p_Agility;
}

int Player::getAgility()
{
	return m_Agility;
}

void Player::addToInventory(Object& p_Object)
{
	m_Inventory.addToInventory(p_Object);
}


void Player::removeFromInventory(Object& p_Object)
{
	m_Inventory.removeFromInventory(p_Object);
}

void Player::removeFromInventory(std::string p_ObjectName)
{
	m_Inventory.removeFromInventory(p_ObjectName);
}

void Player::removeLastObjectFromInventory()
{
	m_Inventory.removeLast();

}

Object Player::getLastObjectInInventory()
{
	return m_Inventory.getLast();
}

bool Player::checkInventory(std::string& p_ObjectName)
{
	return m_Inventory.checkInInventory(p_ObjectName);
}

void Player::setCurrentPosition(int p_x, int p_y)
{
	m_currentX = p_x;
	m_CurrentY = p_y;
}

int Player::getCurrentX()
{
	return m_currentX;
}

int Player::getCurrentY()
{
	return m_CurrentY;
}

Inventory Player::getInventory()
{
	return m_Inventory;
}

bool Player::checkObjectEquiptable(std::string& p_ObjectName)
{
	
	Object toCheck = m_Inventory.getObjectByName(p_ObjectName);
	return toCheck.getEquiptable();

	
}

void Player::printInventory()
{
	m_Inventory.printInventory();
}

Object Player::getEquipt()
{
	return m_Equipt;
}

Object Player::getObjectInInventoryByName(std::string& p_ObjectName)
{
	Object object=m_Inventory.getObjectByName(p_ObjectName);
	return object;
}

void Player::setEquipt(std::string& p_ObjectName)
{
	Object toEquipt = m_Inventory.getObjectByName(p_ObjectName);
	bool found = m_Inventory.removeFromInventory(p_ObjectName);

	if (found == true )
	{
		int objectStrength=toEquipt.getStrength();
		int objectAgility = toEquipt.getAgility();
		//std::cout << objectAgility << std::endl;
		//std::cout << objectStrength << std::endl;
		m_Equipt = toEquipt;
		setAgility(m_Agility + objectAgility);
		setStrength(m_Strength + objectStrength);
		m_hasEquipt = true;
	}

}

void Player::setEquipt(Object& p_object)
{
	m_Equipt = p_object;
}

void Player::setHasEquipt(bool p_hasEquipt)
{
	m_hasEquipt = p_hasEquipt;
}

bool Player::getHasEquipt()
{
	return m_hasEquipt;
}

void Player::removeEquipt()
{
	m_Equipt = Object();
	m_hasEquipt = false;
}

void Player::printEquipt()
{
	std::cout << "Equiptment:" << std::endl;
	std::cout << m_Equipt.getName() << std::endl;
	
}

void Player::printPlayer()
{
	std::cout << "Strength: " << m_Strength;
	std::cout << "	   Agility: " << m_Agility;
	std::cout << "	   Health: " << m_health;
	std::cout << "	   XP: " << m_Xp;
	std::cout << "	   Level: " << m_level << std::endl;
}