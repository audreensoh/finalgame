#ifndef ROOM_H
#define ROOM_H

/**
Contains Room details and attributes and functions to manage rooms.
**/

#include"Object.h"
#include"Enemy.h"
#include<iostream>
#include<vector>

class Room
{
private:
	std::string m_RoomName;
	std::string m_RoomDesc;
	std::vector<Object> m_ObjectsInRoom;
	std::vector<Enemy> m_EnemyInRoom;
	int m_PositionX;
	int m_PositionY;
	bool m_lock;

public:
	Room();
	Room(std::string p_RoomName, std::string p_RoomDesc, int p_positionX, int p_positionY,bool p_lock);
	~Room();

	void setRoomName(std::string p_RoomName);
	std::string getRoomName();
	void setRoomDescription(std::string p_RoomDesc);
	std::string getRoomDescription();
	void setPositionX(int p_positionX);
	int getPositionX();
	void setPositionY(int p_positionY);
	int getPositionY();
	void setLock(bool p_lock);
	bool getLock();

	void addObjectToRoom(Object& p_Object);
	void removeObjectFromRoom(std::string p_Object);
	void removeObjectFromRoom(Object& p_Object);
	std::vector<Object> getAllObject();
	void addEnemyToRoom(Enemy& p_enemy);
	void removeEnemyFromRoom(Enemy& p_enemy);
	void removeLastEnemyInList();
	std::vector<Enemy> getAllEnemy();
	bool checkObjectInRoom(std::string& p_ObjectName);
	Object* getObject(std::string& p_ObjectName);
	void unhideObject();
	void printRoom();

	
};

#endif