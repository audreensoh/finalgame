#include"Enemy.h"

using std::string;

//constructor
Enemy::Enemy()
{}

Enemy::Enemy(std::string p_Name, int p_health, int p_Strength, int p_Agility, int p_x, int p_y, std::string room)
{
	m_Name = p_Name;
	m_health = p_health;
	m_Strength = p_Strength;
	m_Agility = p_Agility;
	m_x = p_x;
	m_y= p_y;
	m_RoomName = room;
}

//Destructor
Enemy::~Enemy()
{}

//getters and setters for enemy
void Enemy::setName(std::string p_Name)
{
	m_Name = p_Name;
}

std::string Enemy::getName()
{
	return m_Name;
}

void Enemy::setHealth(int p_Health)
{
	m_health = p_Health;
}

int Enemy::getHealth()
{
	return m_health;
}

void Enemy::setStrength(int p_Strength)
{
	m_Strength = p_Strength;
}

int Enemy::getStrength()
{
	return m_Strength;
}

void Enemy::setAgility(int p_Agility)
{
	m_Agility = p_Agility;
}

int Enemy::getAgility()
{
	return m_Agility;
}

int Enemy::getX()
{
	return m_x;
}

int Enemy::getY()
{
	return m_y;
}

std::string Enemy::getRoomName()
{
	return m_RoomName;
}